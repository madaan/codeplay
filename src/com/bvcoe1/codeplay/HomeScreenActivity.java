//sg
package com.bvcoe1.codeplay;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.bvcoe1.codeplay.effects.Animator;
import com.bvcoe1.codeplay.effects.SoundManager;
import com.bvcoe1.codeplay.effects.SplashScreenActivity;
import com.bvcoe1.codeplay.scorekeeper.HighScoreActivity;
import com.bvcoe1.codeplay.userhelp.HelpActivity;


public class HomeScreenActivity extends Activity {
	static SoundManager smgr;
	@Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        //Remove title bar
	        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

	        //Remove notification bar
	       this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        
	        setContentView(R.layout.homescreen);
	     Animator.setContext(getApplicationContext());
	         Intent i=new Intent(HomeScreenActivity.this,SplashScreenActivity.class);
        startActivity(i);
        Button start=(Button)findViewById(R.id.buttonStart);
        Button highScore=(Button)findViewById(R.id.buttonHighScore);
        Button help = (Button)findViewById(R.id.buttonHelp);
        Button exit = (Button)findViewById(R.id.buttonExit);
	    smgr=new SoundManager(HomeScreenActivity.this);
	    start.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					Intent i=new Intent(HomeScreenActivity.this,LevelPickerActivity.class);
				
					smgr.playTransition();
					startActivity(i);
				}});
	
        
        help.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					
					Intent intent = new Intent(HomeScreenActivity.this,HelpActivity.class);
					
					smgr.playClick();
					startActivity(intent);
				}
	        	
	        });
	
        
        exit.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					smgr.playClick();
					finish();
				}
        });
        
	    
        highScore.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(HomeScreenActivity.this,HighScoreActivity.class);
					smgr.playClick();	
					startActivity(intent);
	
				}
        });
	
	 }
	@Override
	public void onPause()
	{
		super.onPause();
		Log.d("PAUSE SOUND: ","STOP");
		smgr.stopLoopMusic();
	}
	@Override
	public void onResume()
	{
		super.onPause();
		Log.d("RESUME SOUND: ","START");
		smgr.playLoopMusic();
	}
	


	 }
