//sg
  
package com.bvcoe1.codeplay.scorekeeper;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bvcoe1.codeplay.R;
public class HighScoreListAdapter extends ArrayAdapter<User>
{
	/*The Improvement for Performance*/
	static class ViewHolder
	{
		TextView userName,userScore;
	}
	ViewHolder holder;
	private LayoutInflater myinf;

	
	public HighScoreListAdapter(Context context, int textViewResourceId,
			List<User> users) {
		super(context, textViewResourceId, users);
		
	String inflater=Context.LAYOUT_INFLATER_SERVICE;
	myinf=(LayoutInflater)getContext().getSystemService(inflater);
	}
	
	@Override
	public View getView(int position,View convertView,ViewGroup parent)
	{
		String name;
		Integer score;
		User u=getItem(position);
		score=u.getHighScore();
		name=u.getUserName();
		
		//check if the view is being created for the first time,if so
		//inflate it from the xml
		if(convertView==null)
		{
			 
			convertView=(RelativeLayout)myinf.inflate(R.layout.scoreelement,null);
			holder=new ViewHolder();
			holder.userName=(TextView)convertView.findViewById(R.id.textViewUserHighScoreName);
			holder.userScore=(TextView)convertView.findViewById(R.id.textViewUserHighScoreValue);
			convertView.setTag(holder);
		}
		else
		{
		holder=(ViewHolder)convertView.getTag();
		}
		holder.userName.setText(name);
		holder.userScore.setText(score.toString());
		
		return convertView;
	}
	
}

