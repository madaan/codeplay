//sg


package com.bvcoe1.codeplay.scorekeeper;

import java.util.ArrayList;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**Class To write messages to the SQLite database 
 * this class "HAS A " database writer helper class 
 * to make interaction with the database easier.
 */
public class DBManager {
	
	/**ALWAYS DECLARE ALL CONSTANTS AS PRIVATE STATIC FINAL CLASS VARIABLES*/
	private final static String DATABASE_NAME="highScoreDatabase.db";
	private final static String TABLENAME="highScores";
	private final static String DATABASE_CREATE_QUERY="create table "+TABLENAME+" (sno integer primary key AUTOINCREMENT,userName varchar2(20),userPass varchar2(20),score numeric(5))";
	private final static String DATABASE_DROP_QUERY="DROP TABLE IF EXISTS "+TABLENAME;
	private final static int DATABASE_VERSION=1;
	private static final String columns[]={"sno","userName","userPass","score"};
	/** Like in the case of UserPreference class,the context of the application has to be supplied by the activity using this object*/
	Context c;
	SQLiteDatabase db;
	DBHelper dbh;
	public DBManager(Context c)
	{
	this.c=c;	
	}
	private void openDB()
	{
		
		 dbh=new DBHelper(c,DATABASE_NAME,null,DATABASE_VERSION);
		try
		{
		db=dbh.getWritableDatabase();
		}
		catch(SQLiteException e)
		{
			Log.d("IN DBManager:","ERROR GETTING A WRITABLE DB,TRYING A READABLE");
			db=dbh.getReadableDatabase();
		}
	}
	private void closeDB()
	{
		db.close();
		dbh=null; /** OK for GC now */
	}
	public void addUser(User u)
	{
		/**Extract Values*/
		
		int score=u.getHighScore();
	String userPass=u.getUserPass();
		String userName=u.getUserName();
		
		/**Associate  Values with the respective Columns*/
		ContentValues cv=new ContentValues();
		
		cv.put("userName",userName);
		cv.put("userPass", userPass);
		cv.put("score", score);
		
		
		/**open connection to database*/
		openDB();
		/**Insert the row into the table,null specifies that all the elements are non-null*/
		db.insert(TABLENAME, null, cv);
		/**Finally,close the database*/
	    closeDB();
	}
	public int readAllScores(ArrayList<User> savedScoresList)
	{
		openDB();
	savedScoresList.clear();
		Cursor result=db.query(true, TABLENAME, columns,null, null, null, null, null,null);
		
	if(result.getCount()!=0)   /** Cursor empty ?*/
	{
		result.moveToFirst();
	do
	{
		int sno=result.getInt(0);
		String userName=result.getString(1);
		String userPass=result.getString(2);
		int  score=result.getInt(3);
		savedScoresList.add(new User(sno,userName,userPass,score));
	}while(result.moveToNext());
	closeDB();
	return 1;
	}
	else
	{
		closeDB();
		return -1;
	}
	
	}
	public void deleteAllRecords()
	{
		openDB();
		
		Integer affectedRows=db.delete(TABLENAME, null, null);
		Log.d("delete all",affectedRows.toString()+" ROWS DELETED");
		dbh.onUpgrade(db, DATABASE_VERSION, DATABASE_VERSION);
		closeDB();
	}
	public void deleteBySerialNumber(Integer serialNumber)
	{
		openDB();
		String whereClause="sno="+serialNumber.toString();
		db.delete(TABLENAME, whereClause, null);
		
		closeDB();
	}
	public void deleteByWhereClause(String whereClause)
	{
		openDB();
		db.delete(TABLENAME, whereClause, null);
		closeDB();
	}
	
	public void updateUserHighScore(int userNum,int score)
	{
		ContentValues updated=new ContentValues();
		updated.put("score",score);
		String where=columns[0]+"="+userNum;
		db.update(TABLENAME, updated, where,null);
	
	}
	public User checkUserRecord(String userName)
	{
		/**open the database*/
		openDB();
		/**specify the query to perform */
		String where=columns[1]+"='"+userName+"'";
		/**perform the query*/
		Cursor result=db.query(true, TABLENAME, columns, where, null, null, null, null, null);
		if(result.getCount()==0)
		{
			return new User(-1,"","",0);
		}
		/**extract result*/
		result.moveToFirst();
		int sno=result.getInt(0);
		String userPass=result.getString(2);
		Integer score=result.getInt(3);
		
		return new User(sno,userName,userPass,score);
		
	}
	
/**this nested class has been declared private static and nested
 * due to design and syntax considerations.
 * why static : because we need it's instance but we don't want to create and outer instance first.
 * also we can only refer to statics of the enclosing class (without an instance)
 * page 212 ,http://stackoverflow.com/questions/253492/static-nested-class-in-java-why
 */
	private static class DBHelper extends SQLiteOpenHelper 
	{

	public DBHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE_QUERY);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	   //drop existing table
		db.execSQL(DATABASE_DROP_QUERY);
		//create another one
		onCreate(db);
	}
		
	}

}
