//sg
package com.bvcoe1.codeplay.scorekeeper;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;

import com.bvcoe1.codeplay.R;

public class HighScoreActivity extends Activity {
	private ArrayList<User> highScoresList;
	private HighScoreListAdapter aa;
	private DBManager dbm;
	private ListView lv;
	
	@Override
    public void onCreate(Bundle savedInstanceState) 
{
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
     
        setContentView(R.layout.highscoredisplay);
        dbm=new DBManager(getApplicationContext());
        lv=(ListView)findViewById(R.id.listViewHighScoreList);
        highScoresList=new ArrayList<User>();
        aa=new HighScoreListAdapter(this,R.layout.scoreelement,highScoresList);
        lv.setAdapter(aa);
        readSavedScores();
        
}
	private void readSavedScores()
	{
		if(dbm.readAllScores(highScoresList)==-1) //list empty
		{
		showAlertDialog("No high scores found","Seems like you haven't saved any scores yet","Ok");
		}
		else
		{
		aa.notifyDataSetChanged();
	}
	}
	 private void showAlertDialog(String title,String message,String positiveText)
	 {
		 AlertDialog.Builder alerter=new AlertDialog.Builder(HighScoreActivity.this);
		 alerter.setTitle(title);
		 alerter.setMessage(message);
		 alerter.setPositiveButton(positiveText, new DialogInterface.OnClickListener()
		 {
			 @Override
			 public void onClick(DialogInterface dl,int arg1)
			 {
			
				}
		 }
		 );
		 alerter.show();
	 }
	
}
