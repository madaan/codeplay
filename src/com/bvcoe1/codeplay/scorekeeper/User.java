//sg
package com.bvcoe1.codeplay.scorekeeper;

public class User {

private String userName,userPass;
private int highScore,userNumber;
public User(int userNumber,String name,String pass,int score)
{
	this.userNumber=userNumber;
	this.userName=name;
	this.userPass=pass;
	this.highScore=score;
	
}
/**
 * @return the userNumber
 */
public int getUserNumber() {
	return userNumber;
}

/**
 * @return the userName
 */

public String getUserName() {
	return userName;
}
/**
 * @return the userPass
 */
public String getUserPass() {
	return userPass;
}
/**
 * @return the highScore
 */
public int getHighScore() {
	return highScore;
}


}
