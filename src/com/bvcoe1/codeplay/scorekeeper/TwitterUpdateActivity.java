package com.bvcoe1.codeplay.scorekeeper;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bvcoe1.codeplay.R;
import com.bvcoe1.codeplay.effects.SoundManager;
public class TwitterUpdateActivity extends Activity {
  
   
	Button postTweet;
	private EditText tweet,user;
public static final String TAG="TWITTER UPDATE ACTIVITY";
	private String tweetText,userName;
	private final String CONSUMER_KEY="6gXMtGf9e4mksSYwweeQ",CONSUMER_SECRET="AKJXuGXbznB7WWmUdLiDO8Er2KDo30mmgrUYYSUPfE",
	ACCESS_TOKEN="613346414-9Uoa5BOfnWSEwwB9VaH8CXANsnWL4dLCRZUA21Xm",ACCESS_TOKEN_SECRET="d6FpV9SUbVmmwzYz82tN5aYCbxCb0G0uMMnPERiAv0";
	
	SoundManager smgr;
	@Override
	public void onCreate(Bundle icicle){
		super.onCreate(icicle);
		   this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	      smgr=new SoundManager(TwitterUpdateActivity.this);
		setContentView(R.layout.tweet);
		final Intent i=getIntent();
		
		tweet = (EditText)findViewById(R.id.editTextTweet);
		user = (EditText)findViewById(R.id.editTextTwitterUserName);
		
		postTweet = (Button)findViewById(R.id.buttonPostTweet);
		postTweet.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				userName=user.getEditableText().toString();
				tweetText="@"+userName+" scored "+i.getStringExtra("score")+"!\n";
				tweetText+= tweet.getEditableText().toString();
					smgr.playClick();
				if(tweetText.equals("")){
					Toast.makeText(TwitterUpdateActivity.this,"What do you  want to say? ", Toast.LENGTH_LONG).show();
						}
				else	if(tweetText.length()>=140)
				{
					Toast.makeText(TwitterUpdateActivity.this,"Limit Exceeded : Twitter puts an upper limit of 140 chars on the status ", Toast.LENGTH_LONG).show();
					
				}
				else{
					showAlertDialog("Done","We are talking with the bird to get your tweet up,you can go back and play!","Alright");
					
				new PostTwitterUpdate().execute();
			}
			}
			
		});
	}

	class PostTwitterUpdate extends AsyncTask<Void,Void,Void>
	{
		
	@Override
	public Void doInBackground(Void... x){
		 ConfigurationBuilder cb = new ConfigurationBuilder();
    	 cb.setDebugEnabled(true)
    	   .setOAuthConsumerKey(CONSUMER_KEY)
    	   .setOAuthConsumerSecret(CONSUMER_SECRET)
    	   .setOAuthAccessToken(ACCESS_TOKEN)
    	   .setOAuthAccessTokenSecret(ACCESS_TOKEN_SECRET);
    	 TwitterFactory tf = new TwitterFactory(cb.build());
    	 Twitter twitter = tf.getInstance();
    	 Looper.prepare();
    	
			try {
				Log.d(TAG, "posting status");
				twitter.updateStatus(tweetText);
				} 
			catch (TwitterException e) {
				Toast.makeText(getApplicationContext(),"Error posting tweet,  please check you internet connection and try in a while", Toast.LENGTH_LONG).show();
				smgr.playAlert();
					e.printStackTrace();
			}
			 catch (Exception e) {
					Toast.makeText(getApplicationContext(),"Something wrong has happened, feel free to drop in a mail at \ndeveloper.codeplay@gmail.com", Toast.LENGTH_LONG).show();
					smgr.playAlert();	
						e.printStackTrace();
				}
		
			 return null;
	}
	@Override
	public final void onPostExecute(Void x)
	{
		Toast.makeText(getApplicationContext(),"Tweet posted! \nFind CodePlay at https://twitter.com/#!/Code__Play", Toast.LENGTH_LONG).show();
		smgr.playSuccess();
	}
	}
	 private final void showAlertDialog(String title,String message,String positiveText)
	 {
		 AlertDialog.Builder alerter=new AlertDialog.Builder(TwitterUpdateActivity.this);
		 alerter.setTitle(title);
		 alerter.setMessage(message);
		 alerter.setPositiveButton(positiveText, new DialogInterface.OnClickListener()
		 {
			 @Override
			 public void onClick(DialogInterface dl,int arg1)
			 {
			
				}
		 }
		 );
		 alerter.show();
	 }
	 
}

	