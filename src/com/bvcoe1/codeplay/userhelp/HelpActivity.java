//sg
package com.bvcoe1.codeplay.userhelp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.bvcoe1.codeplay.R;

public class HelpActivity extends Activity {

	public HelpActivity() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		  this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	       
		setContentView (R.layout.help);
		TextView help = (TextView)findViewById(R.id.helptext);
		help.cancelLongPress();
	
	
		Button launchTutorial = (Button)findViewById(R.id.btn_tut);
		launchTutorial.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(HelpActivity.this,TutorialActivity.class);
				startActivity(intent);
			}
			
		});
	}

}
