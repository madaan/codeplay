/**
 * 
 */
package com.bvcoe1.codeplay.userhelp;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.bvcoe1.codeplay.R;
import com.bvcoe1.codeplay.effects.Animator;


public class TutorialActivity extends Activity {

	private  Button translateTutorial,flyTutorial,jumpTutorial,pause;
	private ImageView tutorialAnimationScreen;
	private AnimationDrawable animation;
	private Animator anime;
	private static final int TRANSLATE=1,JUMP=2,FLY=3;
	@Override
	public void onCreate(Bundle icicle){
		super.onCreate(icicle);
		  this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	       
		setContentView(R.layout.tutorial);
		anime=new Animator();
		translateTutorial=(Button)findViewById(R.id.buttonTranslationTutorial);
		jumpTutorial=(Button)findViewById(R.id.buttonJumpTutorial);
		flyTutorial=(Button)findViewById(R.id.buttonFlyTutorial);
		pause=(Button)findViewById(R.id.buttonPauseAnimation);
		tutorialAnimationScreen=(ImageView)findViewById(R.id.imageViewTutorialAnimation);
		pause.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				if(animation.isRunning())
			{
					animation.stop();
			}
			}
        });
		translateTutorial.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				  animateTutorial(TRANSLATE);
			}
        });
	    flyTutorial.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				  animateTutorial(FLY);
			}
        });
	    jumpTutorial.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				  animateTutorial(JUMP);
			}
        });
	}
	private void animateTutorial(int type)
	{
		switch(type)
		{
		case TRANSLATE:
			animation=anime.animateTranslateTutorial();
			break;
		case JUMP:
			animation=anime.animateJumpTutorial();
			break;
		case FLY:
			animation=anime.animateFlyTutorial();
			break;
		}
		Log.d("Tutorial","Launching the tutorial");
		tutorialAnimationScreen.setBackgroundDrawable(animation);
		tutorialAnimationScreen.post(new Starter());
		
	}
	/*
	 * Following function is overridden in all the activities that use graphics 
	 * for performance reasons
	 * */
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		System.gc();
	}
	class Starter implements Runnable {

	    public void run() {
	    	Log.d("Running","Animation");
	        animation.start();        
	    }
	    }
	
}
