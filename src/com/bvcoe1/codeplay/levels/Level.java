//sg
package com.bvcoe1.codeplay.levels;

import com.bvcoe1.codeplay.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public abstract class Level extends Activity {
	protected Button translate,jump,fly,loop,execute,clearAll,clearLastLine,decide;
	protected ImageButton ib;
	protected TextView codeView;
	protected Long startTime,endTime;
	@Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	
	     
    	setContentView(R.layout.main);
        translate=(Button)findViewById(R.id.buttonTranslate);
        codeView=(TextView)findViewById(R.id.textViewCode);
        
        jump=(Button)findViewById(R.id.buttonJump);
        decide=(Button)findViewById(R.id.buttonDecide);
         fly=(Button)findViewById(R.id.buttonFly);
        loop=(Button)findViewById(R.id.buttonRepeat);
        execute=(Button)findViewById(R.id.buttonExecute);
        clearAll=(Button)findViewById(R.id.buttonClearAllCode);
         clearLastLine=(Button)findViewById(R.id.buttonClearLastLine);
        
         ib=(ImageButton)findViewById(R.id.imageButtonLevelDisplay);
        

}
	
}
