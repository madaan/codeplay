//sg
package com.bvcoe1.codeplay.levels;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.bvcoe1.codeplay.R;
import com.bvcoe1.codeplay.codeplayengine.CodePlayInputPaneHandler;
import com.bvcoe1.codeplay.codeplayengine.ExecutionEngineActivity;
import com.bvcoe1.codeplay.effects.SoundManager;
/*
 *  Level 1 : Simple Moving ahead and jumping up
 *  This level simply helps users in getting a hang of what the
 *  game is all about.
 */
public class MoveForwardActivity extends Level {
	private static final int LEVEL_NUMBER=0;
		SoundManager smgr;
		@Override
		public void onCreate(Bundle savedInstanceState)
		{
			super.onCreate(savedInstanceState);
		ib.setImageResource(R.drawable.level1);
    	Integer x,y;
    	smgr=new SoundManager(MoveForwardActivity.this);
    	y=ib.getHeight();
		x=ib.getWidth();
		Log.d("ImageButton Height And Width","("+x.toString()+","+y.toString()+")");
      final CodePlayInputPaneHandler iph=new CodePlayInputPaneHandler(MoveForwardActivity.this,codeView);
      iph.handleClearCode();
       
      //cannot move event listeners to the super class because not every level has same definitions for theses
      
      translate.setOnClickListener(new OnClickListener()
        {
       	@Override
		public void onClick(View v) {
			smgr.playInput();
       		iph.handleTranslate();
		}
      });
       jump.setOnClickListener(new OnClickListener()
       {
		@Override
		public void onClick(View v) {
			smgr.playInput();
			iph.handleJump();
		}
	   });
     ib.setOnClickListener(new OnClickListener()
       {
    	@Override
		public void onClick(View v) {
    		smgr.playInput();
    		iph.handleImageClick(R.drawable.level1measures);
			
    	}
    	   
       });

       loop.setOnClickListener(new OnClickListener()
       {
    	@Override
		public void onClick(View v) {
    		smgr.playInput();
    		iph.handleRepeat();
    	}
    	
       });
       fly.setOnClickListener(new OnClickListener()
       {
    	@Override
		public void onClick(View v) {
    		smgr.playAlert();
    		Toast.makeText(getApplicationContext(),"You cannot fly here!!\nConsider walking my friend\nSimplest solutions are sometimes the best :)", Toast.LENGTH_LONG).show();
    	}
    	
       });
      decide.setOnClickListener(new OnClickListener()
       {
    	@Override
		public void onClick(View v) {
    		smgr.playAlert();
    		Toast.makeText(getApplicationContext(),"No Decision points found!", Toast.LENGTH_LONG).show();
    	}
    	
       });

       clearAll.setOnClickListener(new OnClickListener()
       {
    	@Override
		public void onClick(View v) {
    	smgr.playAlert();
    	iph.handleClearCode();	
    	}
    	});
       clearLastLine.setOnClickListener(new OnClickListener()
       {
    	@Override
		public void onClick(View v) {
    	smgr.playClearLast();
    	iph.handleClearLastLine();	
    	}
    	});
       execute.setOnClickListener(new OnClickListener()
       {
    	@Override
		public void onClick(View v) {
    		smgr.playTransition();
    		String userSolution=iph.getUserSolution();
    		String moves[]=userSolution.split(";");
    		String lastMove=moves[moves.length-1].split(":")[0];
    		if((lastMove.equals("r")))  //last element cannot be a repeat
    		{
    			Toast.makeText(getApplicationContext(),"Last move cannot be repeat!", Toast.LENGTH_LONG).show();
				
    		}
    		else if(userSolution.length()==0)
    		{
    			Toast.makeText(getApplicationContext(),"Testing me,eh?!", Toast.LENGTH_LONG).show();
				
    		}
    		else  //everything is fine, go ahead!
    		{
    		Long elapsedTime;
    		endTime=System.nanoTime();
    		elapsedTime=(endTime-startTime)/(1000000000);//convert to seconds
    		Log.d("Started at : ",startTime.toString());
    		Log.d("Ended at	  : ",endTime.toString());
    		Intent i=new Intent(MoveForwardActivity.this,ExecutionEngineActivity.class);
    		i.putExtra("userSolution", userSolution);
    		i.putExtra("requestingLevel", LEVEL_NUMBER);
    		i.putExtra("timeTaken", elapsedTime);
    		startActivity(i);
    	}
    	}
    	
       });

}	
		
	@Override
	public void onStart()
	{
		super.onStart();
	    startTime=System.nanoTime();
	   
	  //  smgr.playLevelMusic();
		
	}	@Override
	public void onPause()
	{
		super.onPause();
		Log.d("PAUSE SOUND: ","STOP");
	//	smgr.stopLevelMusic();
	}
	
	
    }