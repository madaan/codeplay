//sg
package com.bvcoe1.codeplay.levels;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.bvcoe1.codeplay.R;
import com.bvcoe1.codeplay.codeplayengine.CodePlayInputPaneHandler;
import com.bvcoe1.codeplay.codeplayengine.ExecutionEngineActivity;
import com.bvcoe1.codeplay.effects.SoundManager;

public class JumpAndRepeatActivity extends Level {

	private static final int LEVEL_NUMBER=1;
	private SoundManager smgr;
	@Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
     
   	 	smgr=new SoundManager(JumpAndRepeatActivity.this);
    	ib.setBackgroundResource(R.drawable.l2measuressmall);
        final CodePlayInputPaneHandler iph=new CodePlayInputPaneHandler(JumpAndRepeatActivity.this,codeView);
        iph.handleClearCode();
        
        translate.setOnClickListener(new OnClickListener()
        {
       	@Override
		public void onClick(View v) {
       		smgr.playInput();
			iph.handleTranslate();
		}
      });
       jump.setOnClickListener(new OnClickListener()
       {
		@Override
		public void onClick(View v) {
			smgr.playInput();
			iph.handleJump();
		}
	   });
     ib.setOnClickListener(new OnClickListener()
       {
    	@Override
		public void onClick(View v) {
    		smgr.playInput();
			iph.handleImageClick(R.drawable.l2measures);
		}
    	   
       });

       loop.setOnClickListener(new OnClickListener()
       {
    	@Override
		public void onClick(View v) {
    		smgr.playInput();
    		iph.handleRepeat();
    	}
    	
       });
       fly.setOnClickListener(new OnClickListener()
       {
    	@Override
		public void onClick(View v) {
    		smgr.playInput();
    		iph.handleFly();
    	}
    	
       });
       decide.setOnClickListener(new OnClickListener()
       {
    	@Override
		public void onClick(View v) {
    		smgr.playAlert();
    		Toast.makeText(getApplicationContext(),"No Decision points found!", Toast.LENGTH_LONG).show();
    	}
    	
       });
       clearLastLine.setOnClickListener(new OnClickListener()
       {
    	@Override
		public void onClick(View v) {
    		smgr.playClearLast();
    	iph.handleClearLastLine();	
    	}
    	});
       clearAll.setOnClickListener(new OnClickListener()
       {
    	@Override
		public void onClick(View v) {
    		smgr.playAlert();
    	iph.handleClearCode();	
    	}
    	});
       execute.setOnClickListener(new OnClickListener()
       {
    	@Override
		public void onClick(View v) {
    		smgr.playTransition();
    		String userSolution=iph.getUserSolution();
    		String moves[]=userSolution.split(";");
    		String lastMove=moves[moves.length-1].split(":")[0];
    		if((lastMove.equals("r")))  //last element cannot be a repeat
    		{
    			Toast.makeText(getApplicationContext(),"Last move cannot be repeat!", Toast.LENGTH_LONG).show();
				
    		}
    		else if(userSolution.length()==0)
    		{
    			Toast.makeText(getApplicationContext(),"Testing me,eh?!", Toast.LENGTH_LONG).show();
				
    		}
    		else  //everything is fine, go ahead!
    		{
    		Long elapsedTime;
    		endTime=System.nanoTime();
    		elapsedTime=(endTime-startTime)/(1000000000);//convert to seconds
    		Log.d("Started at : ",startTime.toString());
    		Log.d("Ended at	  : ",endTime.toString());
    		Intent i=new Intent(JumpAndRepeatActivity.this,ExecutionEngineActivity.class);
    		i.putExtra("userSolution", userSolution);
    		i.putExtra("requestingLevel", LEVEL_NUMBER);
    		i.putExtra("timeTaken", elapsedTime);
    		startActivity(i);
    	}
    	}
    	
       });

}	
	@Override
	public void onStart()
	{
		super.onStart();
	    startTime=System.nanoTime();

	    smgr.playLevelMusic();
		
	}	@Override
	public void onPause()
	{
		super.onPause();
		Log.d("PAUSE SOUND: ","STOP");
		smgr.stopLevelMusic();
	}
	

	 
}
