//sg
package com.bvcoe1.codeplay.levels;


	
	import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.bvcoe1.codeplay.R;
import com.bvcoe1.codeplay.codeplayengine.CodePlayInputPaneHandler;
import com.bvcoe1.codeplay.codeplayengine.ExecutionEngineActivity;
import com.bvcoe1.codeplay.effects.SoundManager;
	
		public class DecideLevelActivity extends Level {
		private static final int LEVEL_NUMBER=2;
		SoundManager smgr;
			@Override
			public void onCreate(Bundle savedInstanceState)
			{
				super.onCreate(savedInstanceState);
			ib.setImageResource(R.drawable.level3);
	    	Integer x,y;
	        y=ib.getHeight();
			x=ib.getWidth();
			Log.d("ImageButton Height And Width","("+x.toString()+","+y.toString()+")");
	      final CodePlayInputPaneHandler iph=new CodePlayInputPaneHandler(DecideLevelActivity.this,codeView);
	      iph.handleClearCode();
	        //cannot move event listeners to the super class because not every level has same definitions for theses
	       smgr=new SoundManager(DecideLevelActivity.this);
	       smgr.playLevelMusic();
	 	 
	      translate.setOnClickListener(new OnClickListener()
	        {
	       	@Override
			public void onClick(View v) {
	       		smgr.playInput();
				iph.handleTranslate();
			}
	      });
	       jump.setOnClickListener(new OnClickListener()
	       {
			@Override
			public void onClick(View v) {
				smgr.playInput();
				iph.handleJump();
			}
		   });
	     ib.setOnClickListener(new OnClickListener()
	       {
	    	@Override
			public void onClick(View v) {
				iph.handleImageClick(R.drawable.level3measures);
			}
	    	   
	       });

	       loop.setOnClickListener(new OnClickListener()
	       {
	    	@Override
			public void onClick(View v) {
	    		smgr.playInput();
	    		iph.handleRepeat();
	    	}
	    	
	       });
	       fly.setOnClickListener(new OnClickListener()
	       {
	    	@Override
			public void onClick(View v) {
	    		smgr.playAlert();
	    		Toast.makeText(getApplicationContext(),"You cannot fly here!!\nTake the right decision!", Toast.LENGTH_LONG).show();
		
	    	}
	    	
	       });
	       decide.setOnClickListener(new OnClickListener()
	       {
	    	@Override
			public void onClick(View v) {
	    		ArrayList<String> choices=new ArrayList<String>();
	    		choices.add("If the flag is red,fly 3 units,if it is green fly 10 units");
	    		choices.add("If the flag is yellow,fly 13 units,if it is red fly 3 units");
	    		choices.add("If the flag is green,fly 3 units,if it is red fly 7 units");
	    		smgr.playInput();
	    		iph.handleDecide(choices);
	    	}
	    	
	       });
	       clearAll.setOnClickListener(new OnClickListener()
	       {
	    	@Override
			public void onClick(View v) {
	    		smgr.playAlert();
	    	iph.handleClearCode();	
	    	}
	    	});
	       clearLastLine.setOnClickListener(new OnClickListener()
	       {
	    	@Override
			public void onClick(View v) {
	    	smgr.playClearLast();
	    	iph.handleClearLastLine();	
	    	}
	    	});
	       execute.setOnClickListener(new OnClickListener()
	       {
	    	@Override
			public void onClick(View v) {
	    		
	    		String userSolution=iph.getUserSolution();
	    		String moves[]=userSolution.split(";");
	    		String lastMove=moves[moves.length-1].split(":")[0];
	    		if((lastMove.equals("r")))  //last element cannot be a repeat
	    		{
	    			Toast.makeText(getApplicationContext(),"Last move cannot be repeat!", Toast.LENGTH_LONG).show();
					
	    		}
	    		else if(userSolution.length()==0)
	    		{
	    			Toast.makeText(getApplicationContext(),"Testing me,eh?!", Toast.LENGTH_LONG).show();
					
	    		}
	    		else  //everything is fine, go ahead!
	    		{
	    		Long elapsedTime;
	    		endTime=System.nanoTime();
	    		elapsedTime=(endTime-startTime)/(1000000000);//convert to seconds
	    		Log.d("Started at : ",startTime.toString());
	    		Log.d("Ended at	  : ",endTime.toString());
	    		Intent i=new Intent(DecideLevelActivity.this,ExecutionEngineActivity.class);
	    		i.putExtra("userSolution", userSolution);
	    		i.putExtra("requestingLevel", LEVEL_NUMBER);
	    		i.putExtra("timeTaken", elapsedTime);
	    		smgr.playTransition();
	    		startActivity(i);
	    	}
	    	}
	    	
	       });

	}	
		@Override
		public void onStart()
		{
			super.onStart();
		    startTime=System.nanoTime();
		    smgr.playLevelMusic();
		}
	
		@Override
		public void onPause()
		{
			super.onPause();
			Log.d("PAUSE SOUND: ","STOP");
			smgr.stopLevelMusic();
		}
		
		
	

}
