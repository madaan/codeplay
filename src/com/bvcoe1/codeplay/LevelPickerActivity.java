package com.bvcoe1.codeplay;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.bvcoe1.codeplay.effects.SoundManager;
import com.bvcoe1.codeplay.levels.DecideLevelActivity;
import com.bvcoe1.codeplay.levels.JumpAndRepeatActivity;
import com.bvcoe1.codeplay.levels.MoveForwardActivity;
public class LevelPickerActivity extends Activity {
	 SoundManager smgr;
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        //Remove title bar
	        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        setContentView(R.layout.levelpicker);
	       
	        Button level1=(Button)findViewById(R.id.buttonLevel1);
	        Button level2=(Button)findViewById(R.id.buttonLevel2);
	        Button level3=(Button)findViewById(R.id.buttonLevel3);
	        smgr=new SoundManager(LevelPickerActivity.this);
	        
	        
	        	smgr.playLoopMusic();
	        
	        level1.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					Intent i=new Intent(LevelPickerActivity.this,MoveForwardActivity.class);
				
					smgr.playTransition();
					startActivity(i);
				}
	         });
	        level2.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					Intent i=new Intent(LevelPickerActivity.this,JumpAndRepeatActivity.class);
			
					//smgr.playTransition();
					startActivity(i);
				}
	         });
	        level3.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					Intent i=new Intent(LevelPickerActivity.this,DecideLevelActivity.class);
			
					smgr.playTransition();
					startActivity(i);
				}
	         });	 	
	 }
	 
			@Override
	public void onPause()
	{
		super.onPause();
		Log.d("PAUSE SOUND: ","STOP");
		smgr.stopLoopMusic();
	}
	@Override
	public void onResume()
	{
		super.onPause();
		Log.d("RESUME SOUND: ","START");
		smgr.playLoopMusic();
	}
	

}
