package com.bvcoe1.codeplay.codeplayengine;

import java.util.ArrayList;

public interface GamePaneHandler {
public void handleTranslate();
public void handleJump();
public void handleFly();
public void handleRepeat();
public void handleImageClick(int imageId);
public void handleClearCode();
public void handleClearLastLine();
public void handleDecide(ArrayList<String> choices);
int TRANSLATE=0,JUMP=1,FLY=2,REPEAT=3,DECIDE=4,LINE_DELETE=5,CLEAR_ALL=6;

}
