//sg
package com.bvcoe1.codeplay.codeplayengine;
import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bvcoe1.codeplay.R;
public class CodePlayInputPaneHandler implements GamePaneHandler {
		TextView codeView;
	private static final int MAX_LOC=100;
	private String translateUnits,jumpUnits,flyUnits,repeatCount,repeatLines,decisionNumber,decision;
	private static Integer lineNumber=0;
	/* counts the inputs as they come*/
	private Context requestingLevel;
	boolean inputComplete;
	private boolean lastLoop;
	private Integer repeatCountI;
	private static String userSolution;
	private static String userCode[]; 
	/***clarification 
	userSolution : The solution to be given to the verifier and possibly the renderer to generate the output
	userCode     : The story like code that has to be displayed in central pane
	*
	*/
	
	public CodePlayInputPaneHandler(Context requestingLevel,TextView codeView)
	{
		
		this.codeView=codeView;
		this.inputComplete=false;
	this.requestingLevel=requestingLevel;
	
	userSolution="";
	userCode=new String[MAX_LOC];
	}
	
	
public void handleTranslate()
{
	
	final Dialog d=new Dialog(this.requestingLevel);
	Window w=d.getWindow();
	w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	w.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
	d.setTitle("Translate : Move Ahead");
	d.setContentView(R.layout.translation);
	Button done=(Button)d.findViewById(R.id.buttonTranslateDone);
	final EditText et=(EditText)d.findViewById(R.id.editTextUnitsToTranslate);
	done.setOnClickListener(new OnClickListener()
	{	
	 public void onClick(View v)
		{
			translateUnits=et.getEditableText().toString();
			if(translateUnits.length()==0)
			{
				Toast.makeText(requestingLevel,"Input Expected!", Toast.LENGTH_SHORT).show();
				
			}
			else
			{
		
			updateUserCode(TRANSLATE,translateUnits);
			
			d.dismiss();
			}	
			
			}
	});
	d.show();
	
	/*
	 * It is very important to know the difference between the things that you can ,and the things that you cannot do.
	 * It's not the "nothing is impossible" type of cannot do, it is simply "not possible" type of cannot do.
	 * You cannot break the Async UI model of Android, failing to realize this made me waste over 6 hours.
	 * More on that later. 
	 * 
	 * 
	 * THE FOLLOWING WILL NEVER WORK
	(new AsyncTask<Void,Void,Void>(){

		@Override
		protected Void doInBackground(Void... params) {
			while (!inputComplete)
			{
				Log.d("wait", "waiting");
			}
			return null;
		}
		@Override
		protected void onPostExecute(Void param) {
	     inputComplete=false;
	     }
		
	}).execute();
	
	//start waiting , and wait till the user input is complete
    waitThread.start();
  try {
		waitThread.join();
	} catch (InterruptedException e) {
		
		e.printStackTrace();
	}
	*/
	}

public void handleJump()
{
	final Dialog d=new Dialog(this.requestingLevel);
	Window w=d.getWindow();
	w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	w.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
	d.setTitle("Jump : Climb and Conquer");
	d.setContentView(R.layout.jump);
	Button done=(Button)d.findViewById(R.id.buttonJumpInputDone);
	final
	EditText et=(EditText)d.findViewById(R.id.editTextJumpUnits);
	done.setOnClickListener(new OnClickListener()
	{	
		
		public void onClick(View v)
		{
			jumpUnits=et.getEditableText().toString();
			if(jumpUnits.length()==0)
			{
				Toast.makeText(requestingLevel,"Input Expected!", Toast.LENGTH_SHORT).show();
				
			}
			else
			{
		
			d.dismiss();
			updateUserCode(JUMP,jumpUnits);
		}}
	});
	d.show();
}
public void handleFly()
{
	
	final Dialog d=new Dialog(this.requestingLevel);
	Window w=d.getWindow();
	w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	w.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
	d.setTitle("Translate : Move Ahead");
	d.setContentView(R.layout.fly);
	Button done=(Button)d.findViewById(R.id.buttonFlyInputDone);
	final EditText et=(EditText)d.findViewById(R.id.editTextFlyUnits);
	done.setOnClickListener(new OnClickListener()
	{	
	 public void onClick(View v)
		{
			flyUnits=et.getEditableText().toString();
			if(flyUnits.length()==0)
			{
				Toast.makeText(requestingLevel,"Input Expected!", Toast.LENGTH_SHORT).show();
				
			}
			else
			{
			updateUserCode(FLY,flyUnits);
			d.dismiss();
			}
		}
	});
	d.show();
}
public void handleRepeat()
{
	final Dialog d=new Dialog(this.requestingLevel);
	Window w=d.getWindow();
	w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	w.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
	d.setTitle("Repeat : Persistane is the key");
	d.setContentView(R.layout.looping);
	Button done=(Button)d.findViewById(R.id.buttonRepeatInputDone);
	final EditText etRC=(EditText)d.findViewById(R.id.editTextRepeatCount);
	final EditText etRL=(EditText)d.findViewById(R.id.editTextRepeatLines);
	
	done.setOnClickListener(new OnClickListener()
	{	
	 public void onClick(View v)
		{
			repeatCount=etRC.getEditableText().toString();
			repeatLines=etRL.getEditableText().toString();
			if((repeatCount.length()==0)||(repeatLines.length()==0))
			{
				Toast.makeText(requestingLevel,"Input Expected!", Toast.LENGTH_SHORT).show();
				
			}
			else
			{
			
			updateUserCode(REPEAT,"");
			
			d.dismiss();
			}
			
			}
	});
	d.show();
}
public void handleClearCode()
{
	updateUserCode(CLEAR_ALL,"");
}
public void handleImageClick(int imageId)
{
	final Dialog d=new Dialog(this.requestingLevel);
	Window w=d.getWindow();
	w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	w.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
	d.setTitle("Look Closely!");
	d.setContentView(R.layout.imagefullscreen);
	ImageView iv=(ImageView)d.findViewById(R.id.imageViewLevelFullScreenDisplay);
	  iv.setBackgroundResource(imageId);
	/* animation= (AnimationDrawable) iv.getBackground();
	iv.post(new Starter());
	*/
	//iv.setImageResource(imageId);
	d.show();
}
/*Update the user code
 * ALL THE CHANGES TO CODEVIEW SHOULD TAKE PLACE HERE ONLY!
 */
private void updateUserCode(int type,String units)
{
	Log.d("step 3 ","Updating code with "+units);
	
	switch(type)
	{
	case TRANSLATE :
		if(lastLoop)
		{
			repeatCountI--;
			if(repeatCountI==0)
			{
				lastLoop=false;
			}

			lineNumber++;
			userSolution+="t:"+units+";";
			userCode[lineNumber-1]=lineNumber.toString()+".     Move "+units+" units ahead\n\n";
		 setUserCode();
		}
		else{
		lineNumber++;
		userSolution+="t:"+units+";";
		userCode[lineNumber-1]=lineNumber.toString()+". Move "+units+" units ahead\n\n";
	 setUserCode();
		}
		break;
		
	case JUMP:
		if(lastLoop)
		{
			repeatCountI--;
			if(repeatCountI==0)
			{
				lastLoop=false;
			}
			lineNumber++;
			userSolution+="j:"+units+";";
			userCode[lineNumber-1]=lineNumber.toString()+".     Jump "+units+" units \n\n";
			 setUserCode();
			 
		}
		else
		{
		lineNumber++;
		userSolution+="j:"+units+";";
		userCode[lineNumber-1]=lineNumber.toString()+".Jump "+units+" units \n\n";
		 setUserCode();
			}
		break;
		
	case FLY:
		
		lineNumber++;
		userSolution+="f:"+units+";";
		userCode[lineNumber-1]=lineNumber.toString()+".Fly "+units+" units \n\n";
		setUserCode();
		break;
	
	case REPEAT: 
		repeatCountI=Integer.parseInt(repeatLines);
		lastLoop=true;
		lineNumber++;
		userSolution+="r:"+repeatLines+":"+repeatCount+";";
		userCode[lineNumber-1]=lineNumber.toString()+".Repeat the following "+repeatLines+" lines "+repeatCount+" times \n\n";
		setUserCode();
		break;
	case DECIDE: 
		lineNumber++;
		userSolution+="d:"+decisionNumber+";";
		userCode[lineNumber-1]=lineNumber.toString()+" "+decision +"\n\n";
		setUserCode();
		break;
	
	case LINE_DELETE:
		lineNumber--;
		setUserCode();
		break;
		
	case CLEAR_ALL:
		userSolution="";
		{
			for(int i=0;i<lineNumber;i++)
			{
				userCode[i]="";
			}
		}
		lineNumber=0;
		setUserCode();
		break;
	}
}
/*function that handles the command to clear only last input line
	break the userSolution in on ";",delete the last one
*/
public void handleClearLastLine()
{
	Log.d("Clearing Last Line From",userSolution);
	if(userSolution.length()==0)
	{
		Toast.makeText(requestingLevel,"You haven't entered anything yet, chief", Toast.LENGTH_SHORT).show();
		return;
	}
	String tempSolution[]=userSolution.split(";");
	String newSolution="";
	int l=tempSolution.length;
	for(int i=0;i<l-1;i++) //ignore last
	{
		newSolution+=tempSolution[i]+";";
	
	}
	Log.d("Taking out: ",userCode[lineNumber-1]);
	userSolution=newSolution;
	
	updateUserCode(LINE_DELETE,"");
}
private void setUserCode()
{
	StringBuffer cnctCode=new StringBuffer(1000);
	Log.d("LINE NUMBER",lineNumber.toString());
	for(int i=0;i<lineNumber;i++)
	{
		cnctCode.append(userCode[i]);
	}
	Log.d("Setting user code",cnctCode.toString());
	codeView.setText(cnctCode.toString());
}
public String getUserSolution()
{
	return userSolution;
}


@Override
public void handleDecide(final ArrayList<String> choices) 
{
	
	final Dialog d=new Dialog(this.requestingLevel);
	Window w=d.getWindow();
	ArrayAdapter<String> aa=new ArrayAdapter<String>(requestingLevel,android.R.layout.simple_list_item_1,choices );
	w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	w.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
	d.setTitle("Time to decide!");

	Log.d("here:","to decide");
	d.setContentView(R.layout.decide);
	   ListView decisionList = (ListView)d.findViewById(R.id.listViewChoices);
	   decisionList.setAdapter(aa);
	   
	   decisionList.setBackgroundColor(R.color.bright_text_dark_focused);
	   decisionList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	        @Override
	        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	        decisionNumber=Integer.toString(position);
	        decision=choices.get(position);
	        d.dismiss();
	        updateUserCode(DECIDE,"");
	        }           
	    });
	   d.show();
}
}