//sg
package com.bvcoe1.codeplay.codeplayengine;

/*	Handles the game once the user submits h(is|er) code by pressing execute.
 * 
 * This class takes up the following responsibilities 
 * 
 *  1.  Take the user solution
 *  
 *  2.  calculate the score on the basis of the following parameters :
 *  			# The % correctness of the solution. Though a partially correct code seldom makes any sense,but still it can be considered. 
 *  			# The time taken
 * 	
 *  3. Prompt user to save the high score. If the user is new , then add an entry to the database ,and ask for a password from the user
 * 	   Otherwise, if the score has to be registered for a new user , then ask user to submit a new password along with the user name and 
 * 	   then submit the score to the database.
 * 
 * 4. Formula for calculating score
 * 				 # Score= (%completed)+1000/(time taken)+(1000+1000/(loc))*(finished)
 * 				 Where finished=0 if level has not been completed, 1 otherwise
 */

/*	Flow of control 
 *	
 *This is how the engine works :
 *	1. User completes a level presses execute, the engine wakes up. It takes the user solution ,time taken to solve and the identity of the level
 *	that is requesting the services of the engine (a level number).
 *	
 *	2. As a programming problem can be solve in n number of ways, the first step is to standardize the solution , this includes : 
 *		# Expanding all the loops.
 *		# Compressing all the translations together.
 *
 *	3. Solution is then verified. If it is not totally correct , the % correctness is calculated.
 *	   Score is calculated as per the specifications above.
 *
 *	4. Animate output. 
 *	   Displays the animation depicting the percent completeness of the levels.
 *	   first , frames that must be displayed are calculated and then correct animation is played. 
 * 			 
 * 
 * 
 */
import android.app.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bvcoe1.codeplay.R;
import com.bvcoe1.codeplay.effects.Animator;
import com.bvcoe1.codeplay.effects.SoundManager;
import com.bvcoe1.codeplay.scorekeeper.DBManager;
import com.bvcoe1.codeplay.scorekeeper.TwitterUpdateActivity;
import com.bvcoe1.codeplay.scorekeeper.User;

public class ExecutionEngineActivity extends Activity {

	private static final int LEVEL_1=0,LEVEL_2=1,LEVEL_3=2;
	private static final int FRAMES[]={26,21,13};
	private static final String SOLUTION[]={"t:5;j:3;j:2;t:7;","t:1;j:1;j:1;j:1;j:1;t:2;f:2;t:2;j:1;j:1;t:1;f:2;","j:1;t:1;j:1;t:1;j:1;t:1;d:2;"};
	private Integer requestingLevel;
	private String userSolution;
	private Long timeTaken;
	private Integer score;
	private int loc;
	private Animator anime;
	private TextView scoreOutput;
	private ImageView animationScreen;
	private float percentMatch;
	private AnimationDrawable animation;
	private Button saveScore,shareOnTwitter;
	private SoundManager smgr;
	Button playAnime;
	@Override
	    public void onCreate(Bundle savedInstanceState) 
	{
		score=0;
	        super.onCreate(savedInstanceState);

	        //Remove title bar and notification bar
	        smgr=new SoundManager(ExecutionEngineActivity.this);
	        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        setContentView(R.layout.output);
	        playAnime=(Button)findViewById(R.id.buttonAnimate);
	        scoreOutput=(TextView)findViewById(R.id.textViewScoreTitle);
	        saveScore=(Button)findViewById(R.id.buttonSaveScore);
	        shareOnTwitter=(Button)findViewById(R.id.buttonShare);
		       
	        animationScreen=(ImageView)findViewById(R.id.imageViewAnimation);
	        anime=new Animator();
	        Intent i=getIntent();
	        requestingLevel=i.getIntExtra("requestingLevel", 0);
	        
	        // STEP 1 : 
	        if(requestingLevel==0)
	        {
	        	smgr.playAlert();
	        	Log.d("problem","Wrong value supplied by the caller");
	        }
	        userSolution=i.getStringExtra("userSolution");
	        loc=userSolution.split(";").length;
	        timeTaken=i.getLongExtra("timeTaken",-1);
	        if(timeTaken<0)
	        {
	        	smgr.playAlert();
	        	Log.d("problem","Wrong value supplied by the caller");
		    }
	        Log.d("time: ",timeTaken.toString());
	        Log.d("requestingLevel",requestingLevel.toString());
	      // STEP 2 : 
	        Log.d("userSolution:",userSolution);
		    standardizeSolution();
		    Log.d("standardizedUserSolution:",userSolution);
		    //STEP 3: 
		    verifySolution();
		    //STEP 4: 
	        animateOutput();
	        shareOnTwitter.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					Log.d("Called:","TWITTER");
				//TODO: Add predefined message
					smgr.playClick();
					Intent i=new Intent(ExecutionEngineActivity.this,TwitterUpdateActivity.class);
					if(score!=0)
					i.putExtra("score",score.toString());
					else
					i.putExtra("score","Codeplay : Learning programming the fun way");
					startActivity(i);
			}});
	        saveScore.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						smgr.playClick();
						handleSaveScore(score);// why add more clutter to anonymous class
					}});
		        playAnime.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						smgr.playClick();
						Log.d("here i am", "TO animate");
						  animateOutput();
					       	
					}
		        });
	}
	private void handleSaveScore(final int score)
	{
		
		Button saveScore;
		final EditText userName,userPass;
		final DBManager dbm=new DBManager(getApplicationContext());	

		final Dialog d=new Dialog(this);
		Window w=d.getWindow();
		w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		w.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
		d.setTitle("Make your score count!");
		d.setContentView(R.layout.savescoredialog);
		saveScore=(Button)d.findViewById(R.id.buttonSaveHighScore);
		userName=(EditText)d.findViewById(R.id.editTextUserName);
		userPass=(EditText)d.findViewById(R.id.editTextUserPassword);   
        saveScore.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v)
			{
			/*Now everything will be saved into the SQLite database
			  
			 * The user is asked for a user name and password 
			 * if the user exists already,all his records are returned and then we do 2 things :  
			 * 				-- Match passwords
			 * 				-- Compare Scores : Save new score only if it is greater than 
			 * 				   the previous one.
			 * If the user is new , simply take the entry and then save the user in the database
			 */
				String un,up;
				un=userName.getEditableText().toString();
				up=userPass.getEditableText().toString();
				Log.d("checking user record", un);
				User u=dbm.checkUserRecord(un);
				
				if(u.getUserNumber()==-1)
				{
					// we have a new user , make the id 0 as anyways , the row will be added with new autoincremented unique number
					dbm.addUser(new User(0,un,up,score));
				}
				else
				{
					//the user is pre-existing,check the pass
					if(!u.getUserPass().equals(up))  //wrong password
					{
						smgr.playAlert();
						Log.d("wrong password", "Wrong pass");
						showAlertDialog("Wrong Password!","Please check your password","Ok");
					}
					else if(u.getHighScore()>score) //already a better score
					{
						smgr.playAlert();
						Log.d("ERROR","GOT HIGHER");
						showAlertDialog("Save a lower score?","You have a better score already chief!","Ok");
      
						
					}
					else    //alright , everything is fine, update the record 
						{
						dbm.updateUserHighScore(u.getUserNumber(), score);
						showAlertDialog("Score Saved","Your score has been saved.  Check high scores list at the home screen","Ok");
						smgr.playSuccess();
						d.dismiss();
						}
				}  // end of the case : record already found 
				
			} // handled click
        });
        d.show();
	}
	private void verifySolution()
	{
		boolean finished=false;

		
		switch(requestingLevel)
		{
		case LEVEL_1:
			//translation level
			if(userSolution.equals(SOLUTION[LEVEL_1]))
			{
				finished=true;
				percentMatch=100;
				//correct solution
			}
			else
			{
				percentMatch=matchStrings(userSolution,SOLUTION[LEVEL_1]);
			}
		break;
		case LEVEL_2:
			Log.d("USER_SOLUTION",userSolution);
			Log.d("CORRECT_SOLUTION",SOLUTION[LEVEL_2]);
			
			//looping level
			if(userSolution.equals(SOLUTION[LEVEL_2]))
			{
				finished=true;
				Log.d("solution correct","Level 2");
				percentMatch=100;
				//correct solution
			}
			else
			{
				percentMatch=matchStrings(userSolution,SOLUTION[LEVEL_2]);
			}
		break;	
		case LEVEL_3:
			//looping level
			if(userSolution.equals(SOLUTION[LEVEL_3]))
			{
				finished=true;
				percentMatch=100;
				//correct solution
			}
			else
			{
				percentMatch=matchStrings(userSolution,SOLUTION[LEVEL_3]);
			}
		break;
		}	
		
		// # Score= (%completed)+1000/(time taken)+1000*(finished)
		score=(int)((percentMatch)+1000/timeTaken+(1000+1000/(loc))*((finished)?1:0));
		scoreOutput.setText("    Your Score :" +score.toString()+"   "+Float.toString(percentMatch)+" % Completed");
		if(percentMatch==100)
		{
			smgr.playLevelComplete();
			switch(requestingLevel)
			{
			case 0 : 
				showAlertDialog("Bravo!","You have successfully completed the level!\n2 more to go!","Done!");
				break;	
			case 1 : 
				showAlertDialog("Bravo!","You have successfully completed the level!\n1 more to go!","Done!");
				break;	
			case 2 : 
				showAlertDialog("Bravo!","You have successfully completed the Game!\nWatch out for more levels,Thanks :)","Done!");
				break;	
			
			}
		}
		else
		{
			smgr.playAlert();
			Toast.makeText(getApplicationContext(),"You are missing something!", Toast.LENGTH_LONG).show();
			
		}
		
		
	}
	//match strings
	int matchStrings(String a,String b)
	{
		
		int charsMatch=0;
		int la=a.length()-1;
		int lb=b.length()-1; //the length of the actual solution
		int i=0,j=0;
		while(a.charAt(i)==b.charAt(j))
		{
			charsMatch++;
			if((i++==la)||(j++==lb))
			{
				break;
			}
		}
		//one of a or b has exhausted , we don't care which one , calculate % w.r.t actual solution
		percentMatch=((float)charsMatch/lb)*100;
		
		Log.d("Matched ",Float.toString(percentMatch)+"%");
		return (int)percentMatch;
	}
	
	private void standardizeSolution()
	{
		/*  Expand loops
		 * To go 10 steps ahead , either : 
		 * 					# t:10
		 * 					# write t:x1;...;t:xn; such that x1+x2+...+xn=10 
		 * 					# use r:10;t:1;
		 */
		
	expandLoops();
	/*			Squeeze translates 
	* If you go ahead 5 units , and then in the next step, you go ahead 2 units ahead , then 
	* effective displacement is only 7 units 
	*/
	squeezeTranslations();
	
	}
	
	private void expandLoops()
	{
		String moves[]=userSolution.split(";");
		String expandedSolution="";
		Integer loopVar=0,noStatements;
		Integer len=moves.length;
		int i=0,j=0;
		try
		{
		while(i<len)
		{
		if(moves[i].charAt(0)=='r')
		{
		noStatements=Integer.parseInt(moves[i].split(":")[1]);
		loopVar=Integer.parseInt(moves[i].split(":")[2]);
		
		while((i<len)&&(moves[i+1].charAt(0)=='r')) // checks nesting
		{

		loopVar*=Integer.parseInt(moves[i+1].split(":")[2]);
		i++;
		}
		loopVar=loopVar-1;//the solution is not to be taken
		while(loopVar!=0)
		{
			j=1;
			while(j<=noStatements)
			{
			expandedSolution+=moves[i+j]+";";
			j++;
			}
		loopVar--;
		}
		}
		else
		{
		expandedSolution+=moves[i]+";";
		}
		i++;
		}
		}
		catch(ArrayIndexOutOfBoundsException aob)
		{
			showAlertDialog("OOPS!","Something wrong has happened,please go back and re-enter your code again","Ok");
			
		}
		userSolution=expandedSolution;
		}
	private void squeezeTranslations()
	{
		
		Log.d("Input to squeezer", userSolution);
		try
		{
		String moves[]=userSolution.split(";");
		String squeezedSolution="";
		Integer numberInT=0;
		Integer len=moves.length;
		System.out.println( len.toString());
		int left=0,right=0,i=0;
		while(i<len)
		{
		left=i;
		right=left-1;
		while((i<len)&&(moves[i].charAt(0)=='t')) 
		{
		right=i;
		i++;
		if(i==len)
		{
			break;
		}
		}
		if(right>=left)
		{
		for(int j=left;j<=right;j++)
		{	
		numberInT+=Integer.parseInt(moves[j].split(":")[1]);
		}
		squeezedSolution+="t:"+numberInT+";";
		numberInT=0;
		}

		else
		{
		squeezedSolution+=moves[i]+";";
		i++;
		}
		}
		
		userSolution=squeezedSolution;
		}
		catch(ArrayIndexOutOfBoundsException aob)
		{
			showAlertDialog("OOPS!","Something wrong has happened,please go back and re-enter your code again","Ok");
			
		}
		}
	
	/*private void storyWriter()
	{
		
	}*/
	private void animateOutput()
	{
		Log.d("Animate Output","Animating");
		Integer steps;
		
		switch(requestingLevel)
		{
		case LEVEL_1:
			steps=frameCalculator(requestingLevel);
			Log.d("Animate Till ",steps.toString());
			animation=anime.animateLevelOne(steps);
			break;
		case LEVEL_2:
			steps=frameCalculator(requestingLevel);
			Log.d("Animate Till ",steps.toString());
			animation=anime.animateLevelTwo(steps);
			break;
		case LEVEL_3:
			steps=frameCalculator(requestingLevel);
			Log.d("Animate Till ",steps.toString());
			animation=anime.animateLevelThree(steps);
			break;
		}
		Integer x,y;
		y=animationScreen.getHeight();
		x=animationScreen.getWidth();
		Log.d("My Height And Width","("+x.toString()+","+y.toString()+")");
		animationScreen.setBackgroundDrawable(animation);
		animationScreen.post(new Starter());
	}
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		System.gc();
	}
	class Starter implements Runnable {

	    public void run() {
	        animation.start();        
	    }
	    }
	
	/* Function to calculate how many frames have to be played
	 * taking the percent gives a shorter but sometimes inaccurate solution
	 * instead the following table is now used : 
	 * ################
	 * Level 1 : 
	 * 		frame  8 - t5 over 
	 * 		frame 11 - j3 over
	 * 		frame 13 - j2 over 
	 * 		frame 16 - there is some t
	 * 		last frame - 26th if the level is over 
	 * ################
	 * Level 2:
	 * 		frame 2 : t1
	 * 		frame 3-6 : j1,j1,j1,j1
	 * 		frame 9 : t2	
	 * 		frame 12 : f2
	 * 		frame 13 : t2
	 * 		frame 15: j1 
	 * 		frame 16: j1
	 *		frame 17 :t2
	 *		frame 19 : there is some f
	 *		last frame : 21st level over
	 */
	
	
	 private int frameCalculator(int level)
	 {
		 if(percentMatch==100)
		 {
			 return FRAMES[level]; //no need of calculating
		 }
		 int frames=0;
		 //there are 2 ways to declare a string array. just saying.
		 String solution[]=SOLUTION[level].split(";");
		 String[] temp=userSolution.split(";");
		 int len=temp.length,i;
		 
		 for(i=0;i<len;i++)
		 {
			 if(!(solution[i].equals(temp[i])))
			 {
				 break;
			 }
		 }
		 Log.d("FRAME CALCULATOR,i",Integer.toString(i));
		 Log.d("FRAME CALCULATOR,level",Integer.toString(level));
		 Log.d("FRAME CALCULATOR",Integer.toString(LEVEL_1));
		 if(level==LEVEL_1)
		 {if(i<1)
			 {
				 frames=1;
			 }
			 else if(i==1)
			 {
				 Log.d("SOLVING FOR LEVEL 1","here");
				 frames=8;
			 }
			 else if(i==2)
			 {
				 frames=11;
			 }
			 else if(i==3)
			 {
				 frames=13;
			 }
			 else
			 {
				 frames=17;
			 }
			 //if i were 4 i.e. 100% correct,function would have returned initially
		 }
		
		 else if(level==LEVEL_2)
		 {
			 if(i<=1)
			 {
				 frames=1;
			 }
			 
			 else if(i==2)
			 {
				 frames=3;
			 }
			 else if(i==3)
			 {
				 frames=4;
			 }
			 else if(i==4)
			 {
				 frames=5;
			 }
			 else if(i==5)
			 {
				 frames=6;
			 }
			 else if(i==6)
			 {
				 frames=9;
			 }
			 else if(i==7)
			 {
				 frames=12;
			 }
			 else if(i==8)
			 {
				 frames=13;
			 }
			 else if(i==9)
			 {
				 frames=14;
			 }
			 //t:1;j:1;j:1;j:1;j:1;t:2;f:2;t:2;j:1;j:1;t:2;f:2
			 else if(i==10)
			 {
				 frames=15;
			 }
			 else if(i==11)
			 {
				 frames=17;
			 }
			 else if(temp[i-1].charAt(0)=='f')
			 {
				 frames=19;
			 }
			 else
			 {
				 frames=20;//undefined
			 }
			 //if i were 4 i.e. 100% correct,function would have returned initially
		 }
		 else //LEVEL 3
		 {
			  if(i==1)
			 {
				 frames=2;
			 }
			 else if(i==2)
			 {
				 frames=3;
			 }
			 else if(i==3)
			 {
				 frames=4;
			 }
			 else if(i==4)
			 {
				 frames=5;
			 }
			 else if(i==5)
			 {
				 frames=6;
			 }
			 else if(i==6)
			 {
				 frames=8;
			 }
		 }
		 Log.d("FRAME CALCULATOR ,frames",Integer.toString(frames));
		 return frames;
	 }
	 private void showAlertDialog(String title,String message,String positiveText)
	 {
		 AlertDialog.Builder alerter=new AlertDialog.Builder(ExecutionEngineActivity.this);
		 alerter.setTitle(title);
		 alerter.setMessage(message);
		 alerter.setPositiveButton(positiveText, new DialogInterface.OnClickListener()
		 {
			 @Override
			 public void onClick(DialogInterface dl,int arg1)
			 {
			
				}
		 }
		 );
		 alerter.show();
	 }
	
	}