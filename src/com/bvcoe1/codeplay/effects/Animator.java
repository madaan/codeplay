//sg
package com.bvcoe1.codeplay.effects;

import java.util.Random;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.util.Log;

import com.bvcoe1.codeplay.R;

public class Animator {
	private static final int DELAY=200;
	private static Context context;
private final int level1Drawables[]=
		{R.drawable.l11,R.drawable.l12,R.drawable.l13,R.drawable.l14,R.drawable.l15,R.drawable.l16,R.drawable.l17,R.drawable.l18,R.drawable.l19,
		R.drawable.l110,R.drawable.l111,R.drawable.l112,R.drawable.l113,R.drawable.l114,R.drawable.l115,R.drawable.l116,R.drawable.l117,
		R.drawable.l118,R.drawable.l119,R.drawable.l120,R.drawable.l121,R.drawable.l122,R.drawable.l123,R.drawable.l124,R.drawable.l125,
		R.drawable.l126};
private final int level2Drawables[]=
{
		R.drawable.l21,R.drawable.l22,R.drawable.l23,R.drawable.l24,R.drawable.l25,R.drawable.l26,R.drawable.l27,
		R.drawable.l28,R.drawable.l29,R.drawable.l210,R.drawable.l211,R.drawable.l212,R.drawable.l213,
		R.drawable.l214,R.drawable.l215,R.drawable.l216,R.drawable.l217,R.drawable.l218,R.drawable.l219,
		R.drawable.l220,R.drawable.l221};
private final int level3Case1Drawables[]=
{
		R.drawable.l31,R.drawable.l32,R.drawable.l33,R.drawable.l34,R.drawable.l35,
		R.drawable.l36,R.drawable.l37,R.drawable.l38,R.drawable.l39,R.drawable.l310,
		R.drawable.l311,R.drawable.l312,R.drawable.l313,R.drawable.l3greenreturn
};
private final int level3Case2Drawables[]=
{
		R.drawable.l31,R.drawable.l32,R.drawable.l33,R.drawable.l34,R.drawable.l35,
		R.drawable.l36,R.drawable.l37,R.drawable.l38,R.drawable.l39,R.drawable.l310,
		R.drawable.l311,R.drawable.l312,R.drawable.l318,R.drawable.l319,R.drawable.l320,
		R.drawable.l321,R.drawable.l322,R.drawable.l323,R.drawable.l3bluereturn
};
private final int translateTutorialDrawables[]=
{
		R.drawable.t11,R.drawable.t12,R.drawable.t13,R.drawable.t14,R.drawable.t15,
		R.drawable.t16,R.drawable.t17,R.drawable.t18,R.drawable.t19,R.drawable.t110,
		R.drawable.t111,R.drawable.t112,R.drawable.t113,R.drawable.t114		
		
};

private final int jumpTutorialDrawables[]=
{
		R.drawable.j11,R.drawable.j12,R.drawable.j13,R.drawable.j14,R.drawable.j15,
		R.drawable.j16,R.drawable.j17,R.drawable.j18,R.drawable.j19,R.drawable.j110,
		R.drawable.j111,R.drawable.j112	
};
private final int flyTutorialDrawables[]=
{
		R.drawable.f11,R.drawable.f12,R.drawable.f13,R.drawable.f14,R.drawable.f15,
		R.drawable.f16,R.drawable.f17,R.drawable.f18,R.drawable.f19,R.drawable.f110,
		R.drawable.f111,R.drawable.f112,R.drawable.f113,R.drawable.f114
};
public static void setContext(Context mcontext) {
    if (context == null)
        context = mcontext;
}
public AnimationDrawable animateLevelOne(int stepsComplete)
{
	AnimationDrawable adb=new AnimationDrawable();
	int skip;
	if(stepsComplete>25)
	{
		skip=3;
	}
	else if(stepsComplete>=17)
	{
		skip=2;
	}
	else
	{
		skip=1;
	}
	skip=1;
	for(int i=0;i<stepsComplete;i+=skip)
	{
		adb.addFrame(context.getResources().getDrawable(level1Drawables[i]), DELAY);
	}
	adb.setOneShot(true);
	return adb;
	
}
public AnimationDrawable animateLevelTwo(int stepsComplete)
{
	AnimationDrawable adb=new AnimationDrawable();
	int skip;

	skip=1;
	for(int i=0;i<stepsComplete;i+=skip)
	{
		adb.addFrame(context.getResources().getDrawable(level2Drawables[i]), DELAY);
	}
	adb.setOneShot(true);
	return adb;
	
}
public AnimationDrawable animateLevelThree(Integer stepsComplete)
{
	AnimationDrawable adb=new AnimationDrawable();
	int skip=1;
	Log.d("Animating level 3",stepsComplete.toString());
	if(stepsComplete<=8)
	{
		for(int i=0;i<stepsComplete;i+=skip)
		{
			adb.addFrame(context.getResources().getDrawable(level3Case1Drawables[i]), DELAY);
		}
		adb.setOneShot(true);
	}
	else
	{
		int pickedDrawables[];
		Random r=new Random();
		if(r.nextInt(1000)%2==0)
		{
			pickedDrawables=level3Case1Drawables;
			stepsComplete=14;
		}
		else
		{
			pickedDrawables=level3Case2Drawables;
			stepsComplete=19;
		}
		for(int i=0;i<stepsComplete;i+=skip)
		{
			adb.addFrame(context.getResources().getDrawable(pickedDrawables[i]), DELAY);
		}
		adb.setOneShot(true);
		
	}
	return adb;
	
	
	
}
public AnimationDrawable animateTranslateTutorial()
{
	AnimationDrawable adb=new AnimationDrawable();
	int skip;

	skip=1;
	for(int i=0;i<14;i+=skip)
	{
		adb.addFrame(context.getResources().getDrawable(translateTutorialDrawables[i]), DELAY);
	}
	
	adb.setOneShot(false);
	Log.d("Launching the tutorial","TRANSLATE");
	return adb;
	
}
public AnimationDrawable animateFlyTutorial()
{
	AnimationDrawable adb=new AnimationDrawable();
	int skip;

	skip=1;
	for(int i=0;i<13;i+=skip)
	{
		adb.addFrame(context.getResources().getDrawable(flyTutorialDrawables[i]), DELAY);
	}
	adb.addFrame(context.getResources().getDrawable(flyTutorialDrawables[13]), 1800); //special delay to get the message noticed
	
	adb.setOneShot(false);
	return adb;
	
}
public AnimationDrawable animateJumpTutorial()
{
	AnimationDrawable adb=new AnimationDrawable();
	int skip;

	skip=1;
	for(int i=0;i<12;i+=skip)
	{
		adb.addFrame(context.getResources().getDrawable(jumpTutorialDrawables[i]), DELAY);
	}
	adb.setOneShot(false);
	return adb;
	
}
}

