	//sg
package com.bvcoe1.codeplay.effects;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.bvcoe1.codeplay.R;

public class SplashScreenActivity extends Activity {
	Long delay=3000l;

	
	@Override
	protected void onCreate(Bundle savedInstanceState)  {
		super.onCreate(savedInstanceState);
		 this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	   
		setContentView(R.layout.splashscreen);
	
		Thread splashTread = new Thread() {
	        @Override
	        public void run() {
	            try {
	                sleep(delay);
	                }
	             catch(InterruptedException e) {
	                // do nothing
	            	 }
	             finally {
	                finish();
	               
	            }
	        }
	    };
	    splashTread.start();
	    /*The following won't work as it will block current thread
        try {
            Thread t=Thread.currentThread();
        	Thread.sleep(3000);
            }
         catch(InterruptedException e) {
            // do nothing
        	 }
         finally {
            finish();	
           
        }
*/
	}

	
}
