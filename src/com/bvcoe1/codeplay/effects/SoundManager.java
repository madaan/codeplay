	//sg
package com.bvcoe1.codeplay.effects;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.Log;

import com.bvcoe1.codeplay.R;
public class SoundManager {
	private static SoundPool sounds;
	private static int click,transition,done,input,alert,success,leveldone,clearlast;
	private static boolean loaded;
	public  static MediaPlayer loopMusic,levelMusic;
	//private static MediaPlayer music;	
	public  SoundManager(Context context) {
	if(!loaded)
	{
	    sounds = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
	    
	    click = sounds.load(context, R.raw.button_click, 1);
	    clearlast = sounds.load(context, R.raw.clearlast, 1);
	    
	    transition=sounds.load(context, R.raw.screen_transition, 1);
	    done=sounds.load(context, R.raw.done, 1);
		levelMusic = MediaPlayer.create(context, R.raw.level);
		success=sounds.load(context, R.raw.success, 1);
		alert=sounds.load(context, R.raw.alert, 1);
		input=sounds.load(context, R.raw.input, 1);
		 loopMusic = MediaPlayer.create(context, R.raw.loop);
		 loaded=true;
	}
	}
	public  void playClick() {
	    sounds.play(click, 1, 1, 1, 0, 1);
	}
	public  void playClearLast() {
	    sounds.play(clearlast, 1, 1, 1, 0, 1);
	}
	public  void playInput() {
	    sounds.play(input, 1, 1, 1, 0, 1);
	}
	public  void playTransition() {
	    sounds.play(transition, 1, 1, 1, 0, 1);
	}
	public  void release() {
	    sounds.release();
	    }
	public  void playDone() {
	    sounds.play(done, 1, 1, 1, 0, 1);
	}
	public  void playSuccess() {
	    sounds.play(success, 1, 1, 1, 0, 1);
	}
	public  void playAlert() {
	    sounds.play(alert, 1, 1, 1, 0, 1);
	}
	public  void playLevelComplete() {
	    sounds.play(leveldone, 1, 1, 1, 0, 1);
	}
public final void playLoopMusic() {
    if (!loopMusic.isPlaying()) 
    {	//loopMusic.reset();
    
    	loopMusic.seekTo(0);
    	loopMusic.setLooping(true);
    	loopMusic.start();
    }
}

public  final void stopLoopMusic() {
	Log.d("stop sound","i am called");
    if (loopMusic.isPlaying()) 
    	loopMusic.pause();
}
public final void playLevelMusic() {
    if (!levelMusic.isPlaying()) {
    	levelMusic.seekTo(0);
    	levelMusic.setLooping(true);
    	levelMusic.start();
    }
}
public  final void stopLevelMusic() {
    if (levelMusic.isPlaying()) 
    	levelMusic.stop();
}
}
